from random import randint

def summ(arr):
    s = 0
    for el in arr:
        s += el
    return s

class Universe:
    def __init__(self, n, m, rand = False):
        # Создание вселенной размерности n*m
        if rand:
            self.Universal = [[randint(0,1) for j in range(m)] for i in range(n)]
        else:
            self.Universal = [[0 for j in range(m)] for i in range(n)]

        self.n = n # Высота вселенной (кол-во массивов)
        self.m = m # Длина вселенной (кол-во эл-в массива)

    def __str__(self):
        s = ""
        print('Generation on {}*{}'.format(self.m,self.n))
        for el in self.Universal:
            ls = ' '.join([str(e) for e in el])
            s += ls+'\n'
        return s

    def generation(self, k=0):
        if k == 0:
            print("No generation, Karl!")
            pass
        else:
            for g in range(k):
                # Акт нового поколения
                new_Un = []
                for i in range(self.n):
                    new_Un.append([])
                    for j in range(self.m):
                        i1 = i+1
                        j1 = j+1
                        if i1 > self.n-1:
                            i1 = 0
                        if j1 > self.m-1:
                            j1 = 0
                        s = summ([self.Universal[i-1][j-1],self.Universal[i-1][j],self.Universal[i-1][j1],
                                  self.Universal[i][j-1],self.Universal[i][j1],
                                  self.Universal[i1][j-1],self.Universal[i1][j],self.Universal[i1][j1]])
                        if self.Universal[i][j]:
                            if s == 2 or s == 3:
                                new_Un[i].append(1)
                            else:
                                new_Un[i].append(0)
                        else:
                            if s == 3:
                                new_Un[i].append(1)
                            else:
                                new_Un[i].append(0)
                self.Universal = new_Un

    def expand_universe(self, *args):
        if len(args) == 0:
            for el in self.Universal:
                el.insert(0,0)
                el.append(0)
            self.Universal.insert(0,[0 for i in range(len(self.Universal[0]))])
            self.Universal.append([0 for i in range(len(self.Universal[0]))])
            self.n = self.n+2
            self.m = self.m+2
        else:
            if type(args[0]) is dict:
                dict_ = args[0]
                if dict_['type'] == 'points':
                    pass
                if dict_['type'] == 'arrays':
                    pass
                if dict_['type'] == 'matrix':
                    pass
            

class Planer:

    def __init__(self):
        self.planer = [[0,1,0],
                       [0,0,1],
                       [1,1,1]]
    def __str__(self):
        s =""
        print ("This element is Planer")
        for el in self.planer:
            ls = ' '.join([str(e) for e in el])
            s += ls+'\n'
        return s
